package tools.devnull.jenkins.plugins.buildnotifications;

import hudson.Extension;
import hudson.Launcher;
import hudson.model.AbstractBuild;
import hudson.model.AbstractProject;
import hudson.model.BuildListener;
import hudson.tasks.BuildStepDescriptor;
import hudson.tasks.BuildStepMonitor;
import hudson.tasks.Builder;
import hudson.tasks.Publisher;
import net.sf.json.JSONObject;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.StaplerRequest;

import java.io.IOException;
import java.util.logging.Logger;

public class PreBuildTelegramNotifier extends Builder{

    private final String botToken;
    private final String globalTarget;
    private final String message;

    private static final Logger LOGGER = Logger.getLogger(PreBuildTelegramNotifier.class.getName());

    @DataBoundConstructor
    public PreBuildTelegramNotifier(String globalTarget, String message){
        TelegramDescriptor descriptor = (TelegramDescriptor) getDescriptor();
        this.botToken = descriptor.getBotToken();
        this.globalTarget = globalTarget;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getGlobalTarget() {
        return globalTarget;
    }

    @Override
    public BuildStepMonitor getRequiredMonitorService() {
        return BuildStepMonitor.BUILD;
    }

    @Override
    public boolean perform(AbstractBuild<?, ?> build, Launcher launcher, BuildListener listener)
            throws InterruptedException, IOException {
        send(globalTarget, message);
        return true;
    }

    public void send(String globalTarget, String message) {
        String[] ids = globalTarget.split("\\s*,\\s*");
        HttpClient client = new HttpClient();
        for (String chatId : ids) {
            PostMethod post = new PostMethod(String.format(
                    "https://api.telegram.org/bot%s/sendMessage",
                    botToken
            ));

            post.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");

            post.setRequestBody(new NameValuePair[]{
                    new NameValuePair("chat_id", chatId),
                    new NameValuePair("text", message)
            });
            try {
                client.executeMethod(post);
            } catch (IOException e) {
                LOGGER.severe("Error while sending notification: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    /**
     * The descriptor for the TelegramNotifier plugin
     */
    @Extension
    public static class TelegramDescriptor extends BuildStepDescriptor<Builder> {

        private String botToken;

        public TelegramDescriptor() {
            load();
        }

        public String getBotToken() {
            return botToken;
        }

        @Override
        public boolean configure(StaplerRequest req, JSONObject json) throws FormException {
            JSONObject config = json.getJSONObject("telegram");
            this.botToken = config.getString("botToken");
            save();
            return true;
        }

        @Override
        public boolean isApplicable(Class<? extends AbstractProject> jobType) {
            return true;
        }

        @Override
        public String getDisplayName() {
            return "Telegram Notification";
        }

    }
}
